﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Trigger : MonoBehaviour
{
    [SerializeField] private bool triggerActive = false;
    [SerializeField] public GameObject trigerText;
    public GameObject playerscene1;


    

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerActive = true;
            trigerText.SetActive(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerActive = false;
            trigerText.SetActive(false);
        }
    }

    private void Update()
    {
        if (triggerActive && Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(SomeCoolAction());
            
           GameManager.Instance.player.SetActive(true);
            
          //  playerscene1.SetActive(false);
        }

        IEnumerator SomeCoolAction()
        {
            yield return new WaitForSeconds(1f);
            SceneManager.UnloadSceneAsync("Story1");
            GameManager.Instance.triger.SetActive(true);

        }
   

    }
}
