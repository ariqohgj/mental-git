﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteTrigger : MonoBehaviour
{
    [SerializeField] private bool triggerActive = false;
    [SerializeField] public GameObject trigerText;
    [SerializeField] public GameObject UInote;
    [SerializeField] private bool isView;




    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerActive = true;
            trigerText.SetActive(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerActive = false;
            trigerText.SetActive(false);
        }
    }

    private void Update()
    {
        if (triggerActive  && Input.GetKeyDown(KeyCode.F))
        {


            isView = !isView;

        }
        if (isView)
        {
            ActiveNote();
        }
        else
        {
            DeactiveNote();
        }

    }

    void ActiveNote()
    {
       
            UInote.SetActive(true);
            Time.timeScale = 0;
    
    }

    void DeactiveNote()
    {
        UInote.SetActive(false);
        Time.timeScale = 1;
    }
}
