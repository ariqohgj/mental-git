﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loadscene());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator loadscene()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.UnloadSceneAsync("MainMenu");
    }
}
