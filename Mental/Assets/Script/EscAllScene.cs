﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscAllScene : MonoBehaviour
{
    [SerializeField] private bool escaped = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (escaped && Input.GetKeyDown(KeyCode.Escape))
        {
            escaped = false;
            Time.timeScale = 1;
        }
        else if(!escaped && Input.GetKeyDown(KeyCode.Escape))
        {
            escaped = true;
            Time.timeScale = 0;
        }

    }
}
