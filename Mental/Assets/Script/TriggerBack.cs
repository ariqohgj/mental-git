﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerBack : MonoBehaviour
{
    [SerializeField] private bool triggerActive = false;
    [SerializeField] public GameObject trigerText;
    [SerializeField] public GameObject player;




    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerActive = true;
            trigerText.SetActive(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerActive = false;
            trigerText.SetActive(false);
        }
    }

    private void Update()
    {
        if (triggerActive && Input.GetKeyDown(KeyCode.F))
        {
            
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Additive);
            StartCoroutine(SomeCoolAction());
            GameManager.Instance.player.SetActive(false);
        

        }
    }

   IEnumerator SomeCoolAction()
    {

        
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Story1", LoadSceneMode.Additive);
        yield return new WaitForSeconds(2f);
        GameManager.Instance.triger.SetActive(false);



    }


}
