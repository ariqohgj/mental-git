﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private bool isPaused = false;
    public GameObject pause;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            Deactive();
            isPaused =false;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;

        }
        else if (!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Active();
            isPaused = true;
            Time.timeScale = 0;
            Cursor.visible = true;
            

        }
   //     else
     //   {
     //       StartCoroutine(Active());
       // }
    }

   void Active()
    {
        pause.SetActive(true);
    }

    void Deactive()
    {
        pause.SetActive(false);
    }
}
