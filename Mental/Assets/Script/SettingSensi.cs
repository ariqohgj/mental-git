﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingSensi : MonoBehaviour
{
    public Slider slider;
    public FloatVariable sensi;
    public Text textSensi;

    void Start()
    {
        slider.value = sensi.value;
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void mouseSensi(float NewSensi)
    {
        sensi.value = NewSensi;
        textSensi.text = NewSensi.ToString();
    }
}
