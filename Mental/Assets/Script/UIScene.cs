﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SceneRestart()
    {
        StartCoroutine(UISceneAll());
        Time.timeScale = 1;
    }

    public void Setting()
    {
        SceneManager.LoadScene("Menu", LoadSceneMode.Additive);
    }

    public void BckSetting()
    {
        SceneManager.UnloadSceneAsync("Menu");
    }

    IEnumerator UISceneAll()
    {
        yield return new WaitForSeconds(0f);
        SceneManager.LoadSceneAsync("SampleScene");
    }


}
