﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController controler;

    public float speed = 12f;
    public float gravity = -9.8f;
    public float jumpHeight = 3f;
    

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public GameObject senterLampu;
    


    Vector3 velocity;
    bool isGround;
    bool senterNyala;
    void Awake()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        CharControl();
        senter();

    }

    void CharControl()
    {
        isGround = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGround && velocity.y < 0)
        {
            velocity.y = -2f;
        }



        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controler.Move(move * speed * Time.deltaTime);

        if (Input.GetButtonDown("Jump") && isGround)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controler.Move(velocity * Time.deltaTime);

    }

    void senter()
    {
        
        
        if (Input.GetKeyDown(KeyCode.C))
        {
            senterNyala = !senterNyala;
            
        }
        
        if(senterNyala )
        {
            senterLampu.SetActive(true);
            
        }
        else
        {
            senterLampu.SetActive(false);
        }
    }
}
